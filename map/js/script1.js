﻿/*
  jQuery Document ready
*/
$(function () {
    
    $('#datetimepicker1').datetimepicker(
	{
	    /*
			timeFormat
			Default: "HH:mm",
			A Localization Setting - String of format tokens to be replaced with the time.
		*/

	    timeFormat: "HH:mm:ss",
	    /*
			hourMin
			Default: 0,
			The minimum hour allowed for all dates.
		*/
	    hourMin: 0,
	    /*
			hourMax
			Default: 23, 
			The maximum hour allowed for all dates.
		*/
	    hourMax: 23,
	    /*
			numberOfMonths
			jQuery DatePicker option
			that will show two months in datepicker
		*/
	    numberOfMonths: 1,
	    /*
			minDate
			jQuery datepicker option 
			which set today date as minimum date
		*/
	    minDate: -5000,
	    /*
			maxDate
			jQuery datepicker option 
			which set 30 days later date as maximum date
		*/
	    maxDate: 90,

	    showTimepicker: 0
	});

    $('#datetimepicker2').datetimepicker(
	{
	    /*
			timeFormat
			Default: "HH:mm",
			A Localization Setting - String of format tokens to be replaced with the time.
		*/
	    hour: 23,
	    minute: 59,

	    timeFormat: "HH:mm:ss",
	    /*
			hourMin
			Default: 0,
			The minimum hour allowed for all dates.
		*/
	    hourMin: 0,
	    /*
			hourMax
			Default: 23, 
			The maximum hour allowed for all dates.
		*/
	    hourMax: 23,
	    /*
			numberOfMonths
			jQuery DatePicker option
			that will show two months in datepicker
		*/
	    numberOfMonths: 1,
	    /*
			minDate
			jQuery datepicker option 
			which set today date as minimum date
		*/
	    minDate: -5000,
	    /*
			maxDate
			jQuery datepicker option 
			which set 30 days later date as maximum date
		*/
	    maxDate: 90,

	    showTimepicker: 0
	});


    /*
		below code just enable time picker.
	
    $('#datetimepicker3').timepicker(); - this was Original
    */

    $('#datetimepicker3').timepicker({ defaultValue: '23:59' });
});