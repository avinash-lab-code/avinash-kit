﻿(function () {
    var $section = $('#focal');
    var $panzoom = $section.find('.panzoom').panzoom();
    $panzoom.parent().on('mousewheel.focal',function( e ) {
        e.preventDefault();
        var delta = e.delta || e.originalEvent.wheelDelta;
        var zoomOut = delta ? delta < 0 : e.originalEvent.deltay > 0; 
        $panzoom.panzoom('zoom',zoomOut, {
            animate: false,
            focal : e
        });
    });
})();