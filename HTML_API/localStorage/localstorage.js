// Local storage only work with JSON format so we use JSON.stringify to set data and
// We use JSON.parse method to transform JSON format to javascript object.

console.log(localStorage.length);

console.log('file is loaded');
if (localStorage) {
    console.log('Your browser support localStorage ');
} else {
    console.log('Your browser not support localStorage');
}

function storeDate() {
    var inputnum = document.getElementById('inputnumber').value;
    if (inputnum) {
        var arrayItem = JSON.parse(localStorage.getItem('localArray') || '[]');
        console.log(arrayItem)
        arrayItem.push(JSON.parse(inputnum));
        localStorage.setItem('localArray', JSON.stringify(arrayItem));
    } else {
        alert('Please enter a number');
    }
    console.log(localStorage.length);
}

function clearStorage() {
    localStorage.removeItem('localArray');
    alert('LocalStorage is clear sucssefully. ');
}

function getData() {
    var localArray = JSON.parse(localStorage.getItem('localArray'))
    if (localArray) {
        var printdata = '';
        console.log('array lenght, ', localArray.length);
        for (let i = 1; i <= localArray.length; i++) {
            printdata += localArray[i - 1] + ((localArray.length != i) ? ', ' : '.');
        }
        document.getElementById('localdata').innerHTML = printdata
    } else {
        alert('Local storage is empty');
        document.getElementById('localdata').innerHTML = 'No data found.'
    }
    /*console.log('get data function call');*/
}