// const myObj = {
//   name: 'Avinash',
//   age: 25,
//   favoriteFood: 'Fish'
// };


// console.log(typeof myObj);
// console.log(myObj.length)
// var x = JSON.stringify(myObj);
// console.log(x);
// console.log(typeof x)


const user = {
  name: 'John',
  email: 'john@awesome.com',
  plan: 'Pro'
};

console.log(typeof user);

const userStr = JSON.stringify(user);
console.log(typeof userStr)
console.log(userStr);
// JSON.parse(userStr, function(key,value){
// 	if(typeof value === 'string'){
// 		return value.toUpperCase();
// 	}
// 	return value;
// })


JSON.parse(userStr, (key, value) => {
  if (typeof value === 'string') {
    return value.toUpperCase();
  }else{
  	return value;  	
  }
});
console.log(userStr);
console.log(user);

