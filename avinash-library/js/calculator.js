
var Calculator = function(){
    this.result = 0;
}

Calculator.prototype.add = function(add){
    for(let i=0; i< arguments.length; i++){
        this.result += arguments[i];
    }
    return this;
}

Calculator.prototype.subtract = function(sub){
    for(let i=0; i< arguments.length; i++){
        this.result -= arguments[i];
    }
    return this;
}

Calculator.prototype.printResult = function(){
    return this.result;
}
var sum = new Calculator();

sum.add(5, 7, 5, 8).add(3).subtract(4,5).subtract(3).add(2);

console.log(sum.printResult());

'use strict';
var age = 031;
console.log('age = ' + age);