// This is the simple example of method chaning
console.log('js file is loaded');

var Methods = function() {
	this.name = 'name';
	this.color = 'color';
	this.gender = 'gender';
};


Methods.prototype.setName = function(name) {
	this.name = name;
	return this;
};

Methods.prototype.setColor = function(color) {
	this.color = color;
	return this;
};

Methods.prototype.setGender = function(gender) {
	this.gender = gender;
	return this;
}

Methods.prototype.save = function() {
	console.log('Saving ' +this.name+ ', ' +this.color+ ', ' +this.gender+ ' Methods exuted....');
	return this;
}


var newMethod = new Methods();

newMethod.setName('Avinash');
newMethod.setColor('Blue');
newMethod.setGender('Male');

newMethod.save();


var newTest = new Methods();

newTest.save().setName('Pal').setGender('Female').setColor('Blueberry');


new Methods().setName("Avinash Chand").setColor('Blue').setGender('Male').save();
// newTest.setGender('Female');
// newTest.setColor('red');
// newTest.save();



