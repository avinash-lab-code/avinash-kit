$(document).ready(function(){
  $('.user-enter-head li').click(function(){
    if( !$(this).hasClass('active') ){
      $('.user-enter-head li').removeClass('active');
      $('.user-rel-data > div').removeClass('show-data');
      $(this).addClass('active');
      $('.user-rel-data > div:nth-of-type('+($(this).index()+1)+')').addClass('show-data');
    }
  });

  $('.ham-menu').click(function(){
    $('.navm-wrapper').addClass('side-open');
  });
  $('.close-btn').click(function(){
    $('.navm-wrapper').removeClass('side-open');
  });

  $('#success-close').click(function(){
    $('.success-cont').removeClass('success-visible');
  });

  $('.searched-player').click(function(){
    $(this).remove();
  });
});
